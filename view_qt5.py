#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This file is part of PyQubeVis.

PyQubeVis is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

PyQubeVis is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PyQubeVis.  If not, see <http://www.gnu.org/licenses/>.

Created on 12/2014
@author: Benoît Epinat
"""
import PyQt5
from PyQt5 import QtGui, QtCore, QtWidgets

class View(QtWidgets.QGraphicsView):
    onMouseMoved = QtCore.pyqtSignal()
    onMousePressed = QtCore.pyqtSignal()
    onMouseReleased = QtCore.pyqtSignal()
    onWheel = QtCore.pyqtSignal()
    
    def mousePressEvent(self, event):
        if event.modifiers() == QtCore.Qt.ShiftModifier:
            self.pshift = True
        else:
            self.pshift = False
        if event.modifiers() == QtCore.Qt.ControlModifier:
            self.pctrl = True
        else:
            self.pctrl = False
        self.posp=QtGui.QMouseEvent.pos(event)
        self.onMousePressed.emit()
        
    def mouseReleaseEvent(self, event):
        if event.modifiers() == QtCore.Qt.ShiftModifier:
            self.rshift = True
        else:
            self.rshift = False
        if event.modifiers() == QtCore.Qt.ControlModifier:
            self.rctrl = True
        else:
            self.rctrl = False
        self.posr=QtGui.QMouseEvent.pos(event)
        self.onMouseReleased.emit()
        
    def mouseMoveEvent(self, event):
        self.pos=QtGui.QMouseEvent.pos(event)
        self.onMouseMoved.emit()
        #event.ignore
    
    def wheelEvent(self, event):
        if event.modifiers() == QtCore.Qt.ControlModifier:
            self.delta = event.angleDelta()
            self.onWheel.emit()
        #elif event.modifiers() == QtCore.Qt.ShiftModifier:
            #self.horizontalScrollBar().wheelEvent(event)
        #else:
            #self.verticalScrollBar().wheelEvent(event)
        else:
            QtWidgets.QGraphicsView.wheelEvent(self, event)
    
