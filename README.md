# PyQubeVis

## What is it?

**PyQubeVis** is a GUI to display datacubes. It is particularly adapted for cubes with a small spectral range, as it is the case for Fabry-Perot data.

## Requirements

The maintained version runs under python 3.x. It uses **Qt** librairies and currently works with **Qt5** using **PyQt5**.

**PyQubeVis** uses standard python modules:
sys, os, platform, pyfits, numpy, gzip, matplotlib

A version using **Qt4** (PyQt4) is still maintained and is included in the package as **PyQubeVis_qt4**.

To be executable, PyQubeVis.py has to be in the PATH

## Usage

**PyQubeVis** can be used to visualise cubes in the **adhocw** format. It can also be used to manually clean 2D maps. Using *control* or *shift* allows to clean/select regions.
